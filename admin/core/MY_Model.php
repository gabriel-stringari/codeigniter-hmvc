<?php (defined('BASEPATH')) or exit('No direct script access allowed');

/**
 * CodeIgniter MX_Model
 *
 * Metodos utilizado em todos os models no projeto atual.
 *
 * @package     CodeIgniter
 * @author      Ezoom
 * @subpackage  Model
 * @category    Model
 * @link        http://ezoom.com.br
 * @copyright  Copyright (c) 2008, Ezoom
 * @version 1.0.0
 *
 */
class MY_Model extends CI_Model
{

    /**
     * Modulo acessado
     * @var string
     */
    public $module;

    public $current_module;

    /**
     * Método acessado
     * @var string
     */
    public $method;

    /**
     * Classe acessada
     * @var string
     */
    public $class;

    /**
     * Error retornado pelo DB
     * @var string
     */
    public $error;

    /**
     * Numero do erro retornado pelo DB
     * @var integer
     */
    public $error_number;

    /**
     * Tabela do model
     * @var string
     */
    public $table;

    /**
     * Idioma atual
     * @var string
     */
    public $current_lang = null;

    public $hasCompany = TRUE;

    public function __construct()
    {

        // plural
        $this->load->helper('inflector');
        // delete_file
        $this->load->helper('file');
        // nome da tabela
        $this->_fetch_table();

        $this->current_lang = 1;//$this->current_lang()->id;

        $this->db->simple_query("SET time_zone= '+3:00'");
        $this->db->simple_query("SET lc_time_names= 'pt_BR'");

        $this->db->query("SET SESSION group_concat_max_len = 1000000");

        parent::__construct();
    }

    /**
     * Guess the table name by pluralising the model name
     */
    private function _fetch_table()
    {
        if ($this->table == null) {
            $this->table = plural(preg_replace('/(_m|_model)?$/', '', strtolower(get_class($this))));
        }
    }

    /**
     * Recupera idioma referente ao código
     * @author Ramon Barros <ramon@ezoom.com.br>
     * @date      2015-07-28
     * @copyright Copyright  (c)   2015,         Ezoom
     * @param  string $code
     * @return object
     */
    public function lang($code = null)
    {
        $this->db->select('*')
                 ->from('site_language');
        if (!empty($code)) {
            $this->db->where('code', $code);
        }

        $query = $this->db->get();

        return !empty($code) ? $query->row() : $query->result();
    }

    /**
     * Recupera o idioma atual
     * @author Ramon Barros <ramon@ezoom.com.br>
     * @date      2015-07-28
     * @copyright Copyright  (c)           2015, Ezoom
     * @return object
     */
    public function current_lang()
    {
        return 'pt';//$this->lang($this->lang->lang());
    }

    /**
     * Remove um registro
     * @author Diogo Taparello <diogo@ezoom.com.br>
     * @date      2016-04-01
     * @copyright Copyright  (c) 2015,         Ezoom
     * @param     integer    $id
     * @return    integer
     */
    public function delete($id = 0, $post = array() )
    {
        try {
            $userfiles = dirname(FCPATH) . DS . 'userfiles' . DS . ($this->class != $this->module ? $this->current_module->slug . DS . $this->class : $this->current_module->slug);
            $module = $this->get( array( 'id' => $id ) );

            $delete = false;
            if (!empty($module)) {
                $order_by = false;

                $this->db->trans_start();

                $delete = $this->db->where('id', $id)->delete($this->table);

                if ($delete) {
                    if (isset($module->order_by))
                        $order_by = $module->order_by;

                    $this->hasCompany = $this->hasCompany ? ' and id_company = '.$this->auth->data('company') : '';

                    if( isset($post['images'] ) ){
                        $images = explode('/', $post['images']);
                        foreach ($images as $key => $image) {
                            if( isset($module->$image) && $module->$image != '' )
                                delete_file($userfiles.DS.$module->$image);
                        }
                    }
                    if( isset($post['galleries'] ) ){
                        $galleries = explode('/', $post['galleries']);
                        foreach ($galleries as $key => $gallery) {
                            if (isset($module->$gallery)) {
                                foreach ($module->$gallery as $key => $value) {
                                    if (isset($value->image))
                                        delete_file($userfiles . DS . $value->image);
                                    if (isset($value->file))
                                        delete_file($userfiles . DS . $value->file);
                                }
                            }
                        }
                    }
                    if (isset($module->languages)) {
                        foreach ($module->languages as $lang => $value) {
                            if(isset($images)){
                                foreach ($images as $key => $image) {
                                    if( isset($value->$image) && $value->$image != '' )
                                        delete_file($userfiles.DS.$value->$image);
                                }
                            }
                        }
                    }
                    if ($order_by !== false)
                        $this->db->query('UPDATE '.$this->table.' SET order_by = (order_by - 1) WHERE order_by > '.$this->db->escape($order_by).$this->hasCompany);
                }

                $this->db->trans_complete();
            }

            return $delete;
        } catch (Exception $e) {
            log_message('error', print_r($e, true));
        }
    }

    /**
     * Remove multiplos registros
     * @author Diogo Taparello [diogo@ezoom.com.br]
     * @date   2015-03-27
     * @param  array      $ids
     * @return boolean
     */
    public function delete_multiple($ids = array(), $post = array())
    {
        $delete = array();

        $ids = explode(',', $ids);

        $this->db->trans_start();

        foreach ($ids as $id) {
            /** exclui e armazena o resultado **/
            $delete[$id] = $this->delete($id, $post);
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    /**
     * Ativa/Inativa Banners
     * Author: Diogo Taparello diogo@ezoom.com.br
     * 20.03.2015
     */
    public function sort($data)
    {
        $i = 1;
        foreach ($data['item'] as $value) {
            $this->db->trans_start();
            $this->db->query(
                'UPDATE '.$this->table.'
                SET order_by='.($i + (($data['page'] - 1) * $data['show'])).'
                WHERE id = '.$value
            );
            $this->db->trans_complete();
            $i++;
        }
    }

    /**
     * Insere/Atualiza Vídeos
     * @author Ralf da Rocha [ralf@ezoom.com.br]
     * @date   2016-08-01
     * @param  [array]    $videos [Array com os vídeos a serem inseridos]
     * @param  [int]      $id          [ID do conteúdo]
     * @return [int]               [Quantidade de Vídeos Inseridos]
     */
    public function update_videos($videos, $id)
    {
        $this->db->where($this->foreign_key, $id)->delete( $this->table_videos );
        $insertVideos = array();
        if (!empty($videos)) {
            foreach ($videos as $key => $video) {
                if (trim($video['link'])){
                    $insertVideos[] = array(
                        $this->foreign_key => $id,
                        'id_language' => $video['id_language'],
                        'title' => !empty($video['title']) ? $video['title'] : null,
                        'link' => $video['link']
                    );
                }
            }
            if (!empty($insertVideos)){
                $this->db->insert_batch( $this->table_videos, $insertVideos);
            }
        }
        return count($insertVideos);
    }

    /**
     * Retorna vídeos
     * @author Ralf da Rocha [ralf@ezoom.com.br]
     * @date   2016-08-01
     * @param  [int]      $id          [ID do conteúdo]
     * @param  boolean    $id_language [Língua a ser trazida (FALSE para trazer todas)]
     * @return [type]                  [Array de Objetos]
     */
    public function get_videos($id, $id_language = FALSE)
    {
        $this->db->select('*')
                 ->from($this->table_videos)
                 ->where($this->foreign_key, $id);
        if ($id_language)
            $this->db->where('id_language', $id_language);
        $query = $this->db->get();
        return $query->result();
    }

   /**
    * Retorna imagens da galeria
    * @author Gabriel Stringari [gabriel.stringari@grupoezoom.com.br]
    * @date 2017-11-01
    * @param  [int]     $id    [ID do conteúdo]
    * @param  [string]  $type  [String com nome o type, no caso de haver mais de uma galeria]
    * @return  [type] [Array de Objetos]
    */
    public function get_gallery_images($id, $type = false)
    {
        $this->db->select('gallery.id, gallery.'.$this->foreign_key.', gallery.order_by, gallery.file, gallery.highlighted')
             ->from($this->table_gallery.' as gallery')
             ->where('gallery.'.$this->foreign_key, $id)
             ->order_by('gallery.order_by', 'ASC');

        if(isset($type) && $type)
            $this->db->where('gallery.type', $type);

        if(isset($this->table_gallery_description)) {
            $this->db->select('gallery_desc.subtitle')
                     ->join($this->table_gallery_description.' as gallery_desc', 'gallery_desc.'.$this->foreign_key.'_gallery'.' = gallery.id AND gallery_desc.id_language = '.$this->current_lang);
        } else {
            $this->db->select('gallery.subtitle');
        }

        $query = $this->db->get();
        $data = $query->result();

        if(isset($this->table_gallery_description)) {
            foreach ($data as $key => $value) {
                $data->images[$key]->languages = array();
                $this->db->select('subtitle')
                         ->from($this->table_gallery_description.' as gallery')
                         ->where('gallery.'.$this->foreign_key.'_gallery', $value->id);
                $query = $this->db->get();
                $data[$key]->languages = $query->result();
            }
        }

        return $data;
    }

   /**
    * Grava imagens da galeria
    * @author Gabriel Stringari [gabriel.stringari@grupoezoom.com.br]
    * @date 2017-11-01
    * @param  [array]   $images                     [Array com as imagens a serem inseridos]
    * @param  [int]     $id                         [ID do conteúdo]
    * @return [int]     [Quantidade de imagens inseridos]
    */
    public function insert_gallery_images($images, $id)
    {
        $count = 0;

        foreach ($images as $key => $value) {
            if (is_array($value)) {
                $updateGallery[$this->foreign_key] = $id;
                $updateGallery['file'] = $value['image'];
                $updateGallery['order_by'] = !empty($value['order_by']) ? $value['order_by'] : null;
                $updateGallery['highlighted'] = isset($value['highlighted']) ? 1 : 0;

                if(isset($value['type']) && $value['type'] !== '')
                    $updateGallery['type'] = $value['type'];

                if(!is_array($value['subtitle']))
                    $updateGallery['subtitle'] = !empty($value['subtitle']) ? $value['subtitle'] : null;

                if (!empty($updateGallery)) {
                    $this->db->insert($this->table_gallery, $updateGallery);
                    $count++;
                    $id_gallery = $this->db->insert_id();
                }

                if(is_array($value['subtitle']) && (isset($this->table_gallery_description) && $this->table_gallery_description)) {
                    foreach ($value['subtitle'] as $key => $value) {
                        $updateGalleryDescription = array(
                            $this->foreign_key.'_gallery'  => $id_gallery,
                            'id_language'   => $key,
                            'subtitle'      => $value
                        );
                        $this->db->insert($this->table_gallery_description, $updateGalleryDescription);
                    }
                }
            }
        }

        return $count;
    }

   /**
    * Grava imagens da galeria
    * @author Gabriel Stringari [gabriel.stringari@grupoezoom.com.br]
    * @date 2017-11-01
    * @param  [array]  [Array oldImages___]
    * @return [int]     [Quantidade de imagens atualizados]
    * update_gallery_images($data['oldImagesimages'], $data['oldImagesdetails'], $data['oldImagesimages_outro']);
    */
    public function update_gallery_images()
    {
        $args = func_get_args();
        $count = 0;
        foreach ($args as $oldImages) {
            if (isset($oldImages) && $oldImages && count($oldImages) > 0 && $oldImages !== NULL) {
                foreach ($oldImages as $key => $value) {
                    if(!is_array($value['subtitle']))
                        $updateOldGallery['subtitle'] = $value['subtitle'];

                    $updateOldGallery['highlighted'] = isset($value['highlighted']) ? 1 : 0;

                    if (!empty($updateOldGallery)) {
                        $this->db->where('id', $key)
                                 ->update($this->table_gallery, $updateOldGallery);

                        $count++;
                    }

                    if(is_array($value['subtitle']) && (isset($this->table_gallery_description) && $this->table_gallery_description)) {
                        foreach ($value['subtitle'] as $lang => $value) {
                            $this->db->where($this->foreign_key.'_gallery', $key)
                                     ->where('id_language', $lang)
                                     ->update($this->table_gallery_description, array('subtitle' => $value));
                        }
                    }
                }
            }
        }

        return $count;
    }

    /**
    * Grava arquivos
    * @author Gabriel Stringari [gabriel.stringari@grupoezoom.com.br]
    * @date 2017-11-01
    * @param  [array]   $data                     [Array com as imagens a serem inseridos]
    * @param  [array]   $insert                   [Array com o conteudo a ser inserido]
    * @return [void]
    */
    public function insert_single_file(&$data, &$insert)
    {
        // Imagens
        if (isset($data['file'])){
            foreach ($data['file'] as $name => $file) {
                // Multilinguas
                if (is_array($file)){
                    foreach ($file as $id_language => $value) {
                        $data['value'][$id_language][$name] = $value;
                    }
                }else{
                    $insert[$name] = $file;
                }
            }
        }
    }

    /**
    * Atualiza arquivos
    * @author Gabriel Stringari [gabriel.stringari@grupoezoom.com.br]
    * @date 2017-11-01
    * @param  [array]   $data                     [Array com as imagens a serem inseridos]
    * @param  [array]   $update                   [Arraqy com conteúdo a ser inserdo]
    * @param  [array]   $delete_images            [Array para marcar a serem deletados]
    * @param  [array]   $current                  [Array com o conteúdo atual]
    * @return [void]
    */
    public function update_single_file(&$data, &$update, &$delete_images, $current)
    {
        // Deletar Imagens
        if (isset($data['delete-file'])){
            foreach ($data['delete-file'] as $name => $file) {
                // Multilinguas
                if (is_array($file)){
                    foreach ($file as $id_language => $value) {
                        $data['value'][$id_language][$name] = null;
                        $delete_images[] = $current->languages[$id_language]->{$name};
                    }
                }else{
                    $update[$name] = null;
                    $delete_images[] = $current->{$name};
                }
            }
        }
        // Cadastrar Imagens
        if (isset($data['file'])){
            foreach ($data['file'] as $name => $file) {
                // Multilinguas
                if (is_array($file)){
                    foreach ($file as $id_language => $value) {
                        $data['value'][$id_language][$name] = $value;
                        if (isset($current->languages[$id_language]->{$name}) && $current->languages[$id_language]->{$name}){
                            $delete_images[] = $current->languages[$id_language]->{$name};
                        }
                    }
                }else{
                    $update[$name] = $file;
                    if ($current->{$name}){
                        $delete_images[] = $current->{$name};
                    }
                }
            }
        }
    }

    /**
    * Deleta arquivos
    * @author Gabriel Stringari [gabriel.stringari@grupoezoom.com.br]
    * @date 2017-11-01
    * @param  [array]   $data                     [Array com as imagens a serem inseridos]
    * @param  [array]   $insert                   [Array com o conteudo a ser inserido]
    * @return [void]
    */
    public function delete_single_file(&$delete_images)
    {
       // Confere se possui imagens cadastradas e deleta caso tudo ocorreu certo
        if ( $this->db->trans_status() ) {
            if (!empty($delete_images)){
                foreach ($delete_images as $key => $value) {
                    delete_file(dirname(FCPATH) . DS . 'userfiles' . DS . $this->current_module->slug  . DS . $value);
                }
            }
        }
    }

    /**
    * Busca a slug do (luke i'm (((not))) your) pai
    * @author Gabriel Stringari [gabriel.stringari@grupoezoom.com.br]
    * @date 2017-11-06
    * @param $id                        [int] ID da linha do produto
    * @param $table_parent              [string] Tabela do pai
    * @param $table_parent_description  [string] Tabela description do pai
    * @param $primary_key               [string] Primary Key do pai
    * @param $foreign_key               [string] Foreign Key da relaçao pai - pai_description
    * @return $response                 [Array] Array com as slugs no seu idioma
    */
    public function get_parent_slug($id, $table_parent, $table_parent_description, $primary_key, $foreign_key)
    {
        if (!$id || !$table_parent || !$table_parent_description || !$primary_key || !$foreign_key) {
            return;
        }

        $this->db->select($table_parent_description.'.slug,'.$table_parent_description.'.id_language')
                 ->from($table_parent)
                 ->join($table_parent_description, $table_parent_description.'.'.$foreign_key.' = '.$table_parent.'.'.$primary_key, 'left')
                 ->where($table_parent.'.id_company', $this->auth->data('company'))
                 ->where($table_parent.'.'.$primary_key, $id);

        $query = $this->db->get();
        $slug = $query->result();

        foreach ($slug as $value) {
            $response[$value->id_language] = $value->slug;
        }

        return $response;
    }

    /**
     * Ativa/Inativa Banners
     * Author: Diogo Taparello diogo@ezoom.com.br
     * 20.03.2015
     */
    public function toggleStatus($data)
    {
        $this->db->trans_start();

        $this->db->where('id', $data['id'])
                 ->update($this->table, array($data['type'] => ($data['actived'] == 'true' ? 1 : 0)));

        $this->db->trans_complete();
        return $this->db->trans_status();
    }

    protected function check_null($value)
    {
        return $value = $value ? $value : null;
    }

    /**
     * Recupera o id do ultimo registro
     * @author Ramon Barros [ramon@ezoom.com.br]
     * @date   2015-03-30
     * @param  string  $table
     * @return integer
     */
    public function last_id($table = null)
    {
        if (is_null($table)) {
            $table = $this->table();
        }
        if ($this->db->table_exists($table)) {
            $query = $this->db->query("SHOW TABLE STATUS LIKE '{$table}';");

            return (int) $query->row()->Auto_increment;
        }

        return false;
    }

    /**
     * Recupera o nome da tabela mesmo com alias
     * table t = table
     * database.table t = table
     * database.`table` t = `table`
     * `database`.`table` `t` = `table`
     *
     * @author Ramon Barros [ramon@ezoom.com.br]
     * @date   2015-03-27
     * @param  string $table
     * @return string
     */
    public function _table_name($table = null)
    {
        if (preg_match('@[\.]?([\w-_`]+)\s{1}@', $table, $match)) {
            return end($match);
        }

        return false;
    }

    /**
     * Recupera o alias utilizado na tabela
     * <code>
     *     table.column = false
     *     table.column as c = c
     *     database.table.comun as c = c
     * </code>
     * @author Ramon Barros [ramon@ezoom.com.br]
     * @date   2015-03-27
     * @param  string $table
     * @return string
     */
    public function _table_alias($table = null)
    {
        if (preg_match('@(?:\sas)?\s([\w\W]+)$@', $table, $match)) {
            return end($match);
        } else {
            return $table;
        }

        return false;
    }

    /**
     * Recupera o nome da coluna mesmo com alias
     * <code>
     *     table.column = column
     *     table.column as c = column
     *     database.table.comun as c = column
     * </code>
     *
     * @author Ramon Barros [ramon@ezoom.com.br]
     * @date   2015-03-27
     * @param  string $column
     * @return string
     */
    public function _column_name($column = null)
    {
        if (preg_match('@[\.](\w+)@', $column, $match)) {
            return end($match);
        }

        return false;
    }

    /**
     * Tratamento das mensagens de erro do DB
     * @author Ramon Barros [ramon@ezoom.com.br]
     * @date   2015-03-31
     */
    public function set_error()
    {
        if ($this->db->_error_number()) {
            $this->error = $this->db->_error_message();
            $this->error_number = $this->db->_error_number();
            $this->db->trans_rollback();
            throw new Exception($this->error);
        }

        return false;
    }

    /**
     * Caso o metodo não exista chama um alternativo
     * <code>
     *     $this->table();
     *     $this->table_alias();
     *     $this->primary_key();
     * </code>
     * @author Ramon Barros [ramon@ezoom.com.br]
     * @date   2015-03-27
     * @param  string $name      nome do metodo chamado
     * @param  array  $arguments parametros do metodo
     * @return mixed
     */
    public function __call($method, $parameters)
    {
        if (preg_match('@^(table(?!s))@', $method)) {
            if (strpos($method, '_alias') !== false) {
                $property = str_replace('_alias', '', $method);
                if (property_exists($this, $property)) {
                    return $this->_table_alias($this->$property);
                }
            }
            if (property_exists($this, $method)) {
                if (!empty($this->$method)) {
                    return $this->_table_name($this->$method);
                }
            }
        } elseif (strpos($method, '_key') !== false) {
            if (property_exists($this, $method)) {
                return $this->_column_name($this->$method);
            }
        }
    }

}
