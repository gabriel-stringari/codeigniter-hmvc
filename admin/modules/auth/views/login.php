<div class="login-box card">
    <div class="card-body">
        <?php echo form_open('auth/login', array(
            'class' => 'form-horizontal form-material',
            'id'    => 'loginform'
        )) ?>
            <h3 class="box-title m-b-20">
                <figure class="logo">
                    <img src="<?php echo base_img('logo@2x.png'); ?>" alt="Bah.Digital">
                </figure>
            </h3>

            <div id="infoMessage"><?php echo $message;?></div>
            <div class="form-group ">
                <div class="col-xs-12">
                    <?php echo form_input($identity);?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-12">
                    <?php echo form_input($password);?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12 font-14">
                    <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right">
                        <?php echo T_('Esqueceu sua senha?'); ?>
                    </a>
                </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">
                        <?php echo T_('Entrar'); ?>
                    </button>
                </div>
            </div>
        </form>
        <?php
        echo form_close();
        echo form_open('auth/forgot_password', array(
            'class' => 'form-horizontal',
            'id'    => 'recoverform'
        )) ?>
            <div class="form-group ">
                <div class="col-xs-12">
                    <h3><?php echo T_('Esqueceu sua senha?'); ?></h3>
                    <p class="text-muted">
                        <?php echo T_('Digite o seu endereço de e-mail e nós enviaremos as instruções para você!'); ?>
                    </p>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-xs-12">
                    <input class="form-control" type="text" required placeholder="<?php echo T_('E-mail'); ?>"> </div>
            </div>
            <div class="form-group text-center m-t-20">
                <div class="col-xs-12">
                    <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">
                        <?php echo T_('Recuperar Senha'); ?>
                    </button>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>