<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Olá, <?php echo $loggedUser->first_name; ?>!</h4>
                <p>
                    <?php echo T_('Este sistema foi desenvolvido para que seja uma ferramenta útil e de fácil utilização.
                    Você pode utilizar o menu ao lado para acessar as sessões do seu site, apagar, alterar ou adicionar um novo conteúdo.'); ?>
                </p>

                <div class="icon-welcome">
                    <?php echo load_svg('undraw_organize_resume_utk5'); ?>
                </div>

                <h5 class="card-title">Suporte:</h5>
                <p>
                    <?php echo T_('Se você tiver alguma dúvida, sugestão ou dificuldade ao utilizar o sistema, não exite em nos contatar através de'); ?> <a class="link" href="mailto:suporte@bah.digital">suporte@bah.digital</a>.
                </p>
            </div>
        </div>
    </div>
</div>