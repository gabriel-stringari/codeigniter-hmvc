<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Wee.Digital">

    <title><?php echo $title; ?></title>
    <?php echo $metadata; ?>
    <script type="text/javascript">
        var site_url = '<?php echo site_url(); ?>',
            base_img = '<?php echo base_img(); ?>',
            module = '<?php echo $slug; ?>',
            <?php echo isset($i18n) ? 'i18n = '.$i18n.',' : ''; ?>
            segments = ('<?php echo $this->uri->uri_string(); ?>').split('/'),
            current_lang = '<?php echo $lang; ?>',
            gmaps_key = '<?php echo GMAPS_KEY; ?>';
    </script>

    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('modules/comum/assets/img/favicon.png'); ?>">
    <!-- Style ans Scripts Loaded by Controller -->
    <?php echo $head_styles, $head_scripts; ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div>

    <div id="main-wrapper">

        <?php echo $header, $sidebar; ?>
        <div class="page-wrapper">
            <?php echo $breadcrumb; ?>
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <?php echo $content; ?>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <?php echo $footer ?>
        </div>
    </div>
    <script src="<?php echo base_url("modules/comum/assets/plugins/jquery/jquery.min.js"); ?>"></script>
    <?php echo $body_scripts; ?>
</body>

</html>
