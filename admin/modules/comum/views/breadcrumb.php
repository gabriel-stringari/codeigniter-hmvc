<?php
if (isset($breadcrumb_route) && $breadcrumb_route != false){ ?>
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">
            <?php
                foreach ($breadcrumb_route as $key => $value){
                    if (!$key){
                        echo $value;
                        continue;
                    }
                }
            ?>
            </h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
            <?php
                foreach ($breadcrumb_route as $key => $value){
                    if ($key){ ?>
                        <li class="breadcrumb-item">
                            <a class="link" href="<?php echo site_url($key); ?>"><?php echo $value; ?></a>
                        </li>
                    <?php } else { ?>
                        <li class="breadcrumb-item active">
                            <?php echo $value;?>
                        </li>
                    <?php }
                }
            ?>
            </ol>
        </div>
    </div>
<?php }