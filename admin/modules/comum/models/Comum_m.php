<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comum_m extends MY_Model
{
    public function getUserLogged()
    {
        $usuarioLogado = $this->ion_auth->get_user_id();

        $this->db->select('username, email, first_name, last_name, company, phone, photo')
                 ->from('site_users')
                 ->where('id',  $usuarioLogado);

        $query = $this->db->get();
        return $query->row();

    }
}
