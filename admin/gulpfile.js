/**
 * Caso a compilação do less não esteje funcionando, rodar o seguinte comando no seu htdocs:
 * npm install --save-dev vinyl gulp-dependencies-changed map-stream find-in-file
 */
var pluginsDir = __dirname.replace(/\\/g, '/') + '/../../' + 'node_modules/'

var projectFolder = 'default-modules-frontend'
var pathToDefaultModules = './../' + projectFolder + '/modules/'
var defaultModuleName = 'default'

function loadPlugin(name) {
    if(require.resolve(name)) {
        return require(name)
    }
    return require(pluginsDir + name)
}

var gulp = require('gulp'),
    fs = loadPlugin('fs'),
    argv = loadPlugin('yargs').argv,
    config = {
        paths: {
            defaultModule: pathToDefaultModules + defaultModuleName,
            defaultModuleLess: pathToDefaultModules + defaultModuleName + '/assets/less',
            defaultModuleSass: pathToDefaultModules + defaultModuleName + '/assets/scss',
            bowerDir: 'bower_components',
            // less: {
            //     srcAll:  'modules',
            //     main:    'modules/comum/assets/less/main.less',
            //     mainDir: 'modules/comum/assets/less',
            // },
            sass: {
                srcAll:  'modules',
                main:    'modules/comum/assets/less/main.scss',
                mainDir: 'modules/comum/assets/scss',
            },
            js: {
                srcAll:  ['modules/**/assets/js/*.js','!modules/**/assets/js/*.min.js','!modules/comum/assets/js/*.js']
            },
            comumJs: {
                srcAll:  ['modules/comum/assets/js/*.js','!modules/comum/assets/js/*.min.js']
            },
            maps: {
                srcUrl: '/modules/comum/assets/sourcemaps'
            }
        }
    },
    plugins = {
        cssmin:      loadPlugin('gulp-minify-css'),
        debug:       loadPlugin('gulp-debug'),
        gulpReplace: loadPlugin('gulp-replace'),
        sass:        loadPlugin('gulp-sass'),
        autoprefixer: loadPlugin('gulp-autoprefixer'),
        moment:      loadPlugin('moment'),
        notify:      loadPlugin('gulp-notify'),
        rename:      loadPlugin('gulp-rename'),
        sourcemaps:  loadPlugin('gulp-sourcemaps'),
        util:        loadPlugin('gulp-util'),
        vinyl:       loadPlugin('vinyl'),
        changed:     loadPlugin('gulp-dependencies-changed'),
        map:         loadPlugin('map-stream'),
        findInFile:  loadPlugin('find-in-file'),
        babel:       loadPlugin('gulp-babel')
    };


function getFilesThatContain(file){
    var regex = new RegExp('@import [\'"]?([^\'"]+)?('+file.replace('.','\\.')+')[\'"]?;', "g"),
        files = [],
        promises = [];
    return new Promise(
        (resolve) => {
            gulp.src(['modules/**/*.scss', '!**/_*/**/*.scss'])
                .pipe(plugins.map(function (file, cb) {
                    plugins.findInFile({
                        files: file.path,
                        find: regex
                    }, function(err, matchedFiles) {
                        if (matchedFiles.length){
                            matchedFiles.forEach((data) => {
                                var f = data.file.split('\\').pop();
                                if ((m = /^_/g.exec(f)) !== null) {
                                    promises.push(getFilesThatContain(f).then((fs)=>{
                                        files = files.concat(fs);
                                    }));
                                }else{
                                    files.push(data.file);
                                }
                            });
                        }
                    });
                    cb(null, file);
                })).on('end', function(){
                    Promise.all(promises).then(function(values) {
                        resolve(files)
                    });
                });
        }
    );
}

function css(file){
    var sassFile = new plugins.vinyl({ path: file.path }),
        basePath = (sassFile.path).replace(sassFile.relative,''),
        sassPromise = new Promise((resolve) => {
            // Confere se arquivo começa com _
            if ((m = /^_/g.exec(sassFile.basename)) !== null) {
                getFilesThatContain(sassFile.basename).then((files)=>{
                    resolve(files);
                });
            }else{
                // Pega o arquivo a ser alterado
                resolve([sassFile.path]);
            }
        }).then((files)=>{
            // Compila e minifica os arquivos selecionados até aqui:
            return gulp.src(files, { base: 'modules' })
                    .pipe(
                        plugins.sass({
                            paths: [
                                '.',
                                './node_modules',
                                './bower_components',
                                'modules/comum/assets/scss'
                            ]
                        }).on("error", plugins.notify.onError(function (error) {
                            return "Error: " + error.message + '\n' + error.line + ':' + error.index
                        }))
                    )
                    .pipe(plugins.cssmin({compatibility:'ie9'}))
                    .pipe(plugins.rename(function(path) {
                        path.dirname += '/../css';
                    }))
                    .pipe(plugins.autoprefixer({
                        browsers: ['last 2 versions'],
                        cascade: false
                    }))
	                .pipe(gulp.dest('modules'))
                    .pipe(plugins.debug({title:'Compiled SCSS: '}))
                    .pipe(plugins.notify({
                        message:'Compiled SCSS',
                        onLast:true
                    }));
        });
};

// var sass = require('gulp-sass');
gulp.task('sass', function () {
	return gulp.src([config.paths.sass.srcAll + '/**/[^_]*.scss', '!application/modules/comum/assets/plugins/**/*.scss'])
		.pipe(plugins.sass({
				paths: ['.',
					'./node_modules',
					'./bower_components',
					'application/modules/comum/assets/scss'
				]
			})
			.on("error", plugins.notify.onError(function (error) {
				return "Error: " + error.message + '\n' + error.line + ':' + error.index
            })))
        .pipe(plugins.cssmin({
        	compatibility: 'ie9'
        }))
		.pipe(plugins.rename(function (path) {
			path.dirname += '/../css';
		}))
		.pipe(gulp.dest(config.paths.sass.srcAll))
		.pipe(plugins.debug())
		.pipe(plugins.notify({
			message: 'Compiled SCSS (' + plugins.moment().format('MMM Do h:mm:ss A') + ')',
			onLast: true
		}));
});

//Watches for changes and executes the tasks automatically
gulp.task('watch', function() {
    //plugins.livereload.listen()
    gulp.watch(config.paths.sass.srcAll + '/**/*.scss', css);
    gulp.watch(config.paths.js.srcAll, ['compress'])
    gulp.watch(config.paths.comumJs.srcAll, ['comumjs'])
    // gulp.watch(config.paths.sass.srcAll + '/**/*.scss', ['sass'])
});

gulp.task('comumjs', function() {
  var concat = require('gulp-concat'),
      minify = require(pluginsDir + 'gulp-minify');

  gulp.src([
        'modules/comum/assets/js/Exceptions.js',
        'modules/comum/assets/js/Global.js',
        'modules/comum/assets/js/Url.js',
        'modules/comum/assets/js/Comum.js',
        'modules/comum/assets/js/Main.js',
        '!modules/comum/assets/js/*.min.js'
    ])
    .pipe(concat('main.js'))
    .pipe(minify({
        ext:{
            min:'.min.js'
        },
        noSource: true,
        ignoreFiles: ['.min.js', '-min.js'],
        mangle: {
            reserved: ['Exceptions', 'Comum', 'Url']
        }
    }).on('error', plugins.notify.onError({
        message: "Error comumjs: <%= error.message %>",
        title: "Syntax error"
    })))
    .pipe(gulp.dest('modules/comum/assets/js/'))
    .pipe(plugins.notify({
        message:'Compressed main.js (' + plugins.moment().format('MMM Do h:mm:ss A') + ')',
        onLast:true
    }));
});

gulp.task('compress', function() {
    var minify = require(pluginsDir + 'gulp-minify');
    gulp.src(config.paths.js.srcAll)
        .pipe(plugins.babel({
            presets: ['env']
        }))
        .pipe(minify({
            ext:{
                min:'.min.js'
            },
            noSource: true,
            ignoreFiles: ['.min.js', '-min.js']
        }).on('error', plugins.notify.onError({
            message: "Error: <%= error.message %>",
            title: "Syntax error"
        })))
        .pipe(gulp.dest('modules'))
        .pipe(plugins.notify({
            message:'Compressed JS (' + plugins.moment().format('MMM Do h:mm:ss A') + ')',
            onLast:true
        }));
});

gulp.task('default', ['watch','compress','comumjs']);
gulp.task('dev', ['watch']);
gulp.task('dist', ['compress']);
