<?php (defined('BASEPATH')) or exit('No direct script access allowed.');

/**
 * Adiciona a funcionalidade de limpar o input->post
 */
class MY_Form_validation extends CI_Form_validation
{
    /**
     * Reescrito método is_unique para dar suporte a verificação durante a edição
     *
     * is_unique[tabela.coluna]       Valida a coluna
     * is_unique[tabela.coluna.id.1]  valida a coluna e id diferente de 1
     *
     * @param  [string] $str
     * @param  [string] $field
     * @return boolean
     */
    public function is_unique($str, $field)
    {
        if (substr_count($field, '.') == 3) {
            list($table,$field,$id_field,$id_val) = explode('.', $field);
            $query = $this->CI->db->limit(1)->where($field, $str)->where($id_field.' != ', $id_val)->get($table);
        } else {
            list($table, $field)=explode('.', $field);
            $query = $this->CI->db->limit(1)->get_where($table, array($field => $str));
        }

        return $query->num_rows() === 0;
    }

    /**
     * Permite a utilização de callback no mesmo controller que chamou
     * @param  string     $module
     * @param  string     $group
     * @return [callback]
     */
    public function run($module = '', $group = '')
    {
        (is_object($module)) and $this->CI = &$module;

        return parent::run($group);
    }

    /**
     * Limpa todos os campos
     * @return [none]
     */
    public function unset_field_data()
    {
        unset($this->_field_data);
    }

    /**
     * Retorna erros no formado de array
     * @param  boolean    $keys Se setado para TRUE retorna apenas as chaves onde conteve erros, se não o array completo
     * @return [array]
     */
    function error_array($keys = FALSE)
    {
        return ($keys) ? array_keys($this->_error_array) : $this->_error_array;
    }
}
