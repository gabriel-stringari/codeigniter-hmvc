<!doctype html>
<html lang="<?php echo $lang; ?>">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="theme-color" content="#2D6BA1" />
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $title; ?></title>
    <?php echo $metadata; ?>
    <script type="text/javascript">
        var site_url = '<?php echo site_url(); ?>',
            base_img = '<?php echo base_img(); ?>',
            module = '<?php echo $slug; ?>',
            <?php echo isset($i18n) ? 'i18n = '.$i18n.',' : ''; ?>
            segments = ('<?php echo $this->uri->uri_string(); ?>').split('/'),
            current_lang = '<?php echo $lang; ?>',
            gmaps_key = '<?php echo GMAPS_KEY; ?>';
    </script>
    <link rel="shortcut icon"  type="image/x-icon" href="<?php echo base_url('modules/comum/assets/img/favicon.ico'); ?>">
    <?php echo $head_styles, $head_scripts; ?>
    <?php //<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script> ?>
</head>
<body class="preload">
    <?php echo $sidebar, $header; ?>
    <section class="containerWithSidebar">


        <?php echo $breadcrumb; ?>
        <div class="containerBorder">
            <?php echo $content; ?>
        </div>
    </section>
    <?php echo $footer; ?>


    <script src="<?php echo base_url("modules/comum/assets/plugins/jquery-1.11.0.min.js"); ?>"></script>
    <?php echo $body_scripts; ?>
</body>
</html>
