<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {
    protected $isMobile = FALSE;
    protected $module   = FALSE;
    protected $class    = FALSE;
    protected $method   = FALSE;
    protected $slug     = FALSE;
    protected $lang     = FALSE;
    protected $i18n     = array();

    public function __construct()
    {
        parent::__construct();
        // Carrega o template
        $this->load->library('template');

        //Inicia o UserAgent para ver de onde vem
        $this->load->library('user_agent');
        $this->load->helper('language');

        //Inicializa a library Whoops e ativa o profile (se configurado no config.php) em ambiente de desenvolvimento
        if (ENVIRONMENT == 'development') {
            $this->load->library('Whoops');
            $this->output->enable_profiler($this->config->item('enable_profiler'));
        }

        //Setamos algumas informações no controller
        $this->lang   = 'pt'; //$this->lang->lang();;
        $this->module = $this->router->fetch_module();
        $this->class  = $this->router->fetch_class();
        $this->method = $this->router->fetch_method();
        $this->slug   = $this->module == $this->class
                            ? str_replace('_', '-', $this->module)
                            : str_replace('_', '-', $this->module.'/'.$this->class);

        if($this->config->item('support_mobile')){
            $this->isMobile = $this->agent->is_mobile();
        }else{
            $this->isMobile = FALSE;
        }

        //Carrega as configurações padrões
        $this->load_default();

        //Carrega as configurações do módulo
        // $this->load_module();
    }

    /*
    / Carrega os arquivos padrões, como partials, css e JS
    */
    public function load_default(){
        if (!$this->input->is_ajax_request())
        {
            $this->template
                 ->add_css('css/main', 'comum')
                 ->add_js('plugins/plugins', 'comum')
                 ->set('logo', '')
                 ->set('title', 'Dashboard | Admin')
                 ->set_partial('header', 'header', 'comum')
                 ->set_partial('sidebar', 'sidebar', 'comum')
                 ->set_partial('breadcrumb', 'breadcrumb', 'comum')
                 ->set_partial('footer', 'footer', 'comum')
                 ->set('lang', $this->lang)
                 ->set('isMobile', $this->isMobile())
                 ->set('class', $this->class)
                 ->set('module', $this->module)
                 ->set('slug', $this->slug)
                 ->set('version', 'v'.$this->config->item('version'))
                 ->set('csrf_test_name', $this->security->get_csrf_hash())
                 ->set('i18n', $this->_get_js_translation());
                //  ->set('order_by', $this->session->flashdata('order_by'))
                //  ->set('languages', $this->languages)
                //  ->set('all_companies', $this->all_companies)
                //  ->set('company', $this->company)
                //  ->set('current_module', $this->current_module)
                //  ->set('sidebar_menu', $this->auth->create_menu(substr($this->uri->uri_string(), 1), '', true))
                //  ->set('session_permissions', $this->auth->get_session_permissions());
        }
    }

    /*
    / Carrega os arquivos css e JS do module
    */
    public function load_module()
    {
        $this->title .= ' - ' . ucfirst($this->module) . '/' . ucfirst($this->class);

        switch ($this->method) {
            case 'index':
            case 'pagina':
                $this->view = "{$this->class}/listagem";
                break;
            default:
                $this->view = "{$this->class}/{$this->method}";
                break;
        }

        $file = FCPATH . 'modules' . DS . $this->module . DS . 'models' . DS . $this->class . '_m' . EXT;
        if (file_exists($file)) {
            // Carregamento do model
            $this->load->model("{$this->class}_m");

            $this->model = $this->{$this->class . '_m'};

            $this->model->module = $this->module;
            $this->model->current_module = $this->current_module;
            $this->model->class = $this->class;
            $this->model->method = $this->method;
        }

        /**
         * Verifica se a requisição não é ajax
         * X-Requested-With: XMLHttpRequest
         */
        if (!$this->input->is_ajax_request()) {
            $this->template
                 ->add_css("css/{$this->module}")
                 ->add_css("css/{$this->class}")
                 ->add_js("js/{$this->module}")
                 ->add_js("js/{$this->class}");
        }
    }

    public function isMobile()
    {
        return (isset($this->isMobile) && $this->isMobile) ? TRUE : FALSE;
    }

    private function _get_js_translation()
    {
        $js = array_merge(
            array(
                'ajax_error_message' => T_('Desculpe, ocorreu um problema inesperado. Confira os campos preenchidos no formulário e tente novamente.'),
                'ajax_error_title' => T_('Ocorreu um erro'),
                'close' => T_('Fechar')
            ),
            $this->i18n
        );

        return json_encode($js);
    }

    private function _js_translation()
    {
        $js = array(

        );

        return json_encode($js);
    }
}